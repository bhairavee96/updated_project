from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from Employee.views import PersonView, CompanyView

urlpatterns = [
    url(r'^Employee/PersonView/$', csrf_exempt(PersonView.as_view())),
    url(r'^Employee/CompanyView/$', csrf_exempt(CompanyView.as_view())),
]