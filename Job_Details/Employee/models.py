from __future__ import unicode_literals
from django.db import models

class Company(models.Model):
	company_name = models.CharField(max_length=25, null=True, blank=True)
	location = models.CharField(max_length=25, null=True, blank=True)
	pin_code = models.CharField(max_length=10, null=True, blank=True)
	
	def __unicode__(self):
		return str(self.company_name) + ' ( ' + str(self.location) + ' ) '+ '\n'

class Person(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50, null=True, blank=True)
	user_name = models.CharField(max_length=50, unique=True)
	company_name = models.ForeignKey(Company,on_delete = models.CASCADE)

	def __unicode__(self):
		return str(self.first_name) + ': '  + str(self.company_name) + '\n'  