from __future__ import unicode_literals
from models import Person
from models import Company
from django.contrib import admin

admin.site.register(Person)
admin.site.register(Company)