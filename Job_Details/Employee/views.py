from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, QueryDict
import json
from django.core.exceptions import ObjectDoesNotExist
from Employee.models import Person, Company

class PersonView(View):
	def __init__(self):
		print 'Person View Working'

	def get(self, request, *args, **kwargs):
		_params = request.GET
		#persons = Person.objects.exclude(first_name=first_name) #it will print every name except that given in url
		#persons = Person.objects.filter(first_name=first_name) #it will print only that name given in url
		persons_array = Person.objects.all()
		data=[]
		for person in persons_array:
			data.append({
				'first_name': person.first_name,
				'last_name': person.last_name,
				'user_name': person.user_name,
				'company_name': person.company_name.company_name
				})
		return HttpResponse(json.dumps(data), content_type='application/json')

	def put(self, request, *args, **kwargs):
		_arg1 = QueryDict(request.body)
		first_name = _arg1.get('first_name')
		last_name = _arg1.get('last_name')
		user_name = _arg1.get('user_name')
		company_name = _arg1.get('company_name')

		try:
			per = Person.objects.get(user_name=user_name)
		except ObjectDoesNotExist:
			return HttpResponse("User does not exist")
		try:
			company_obj = Company.objects.get(company_name=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Company does not exist")
		per.company_name=company_obj
		per.first_name = first_name
		per.last_name = last_name
		per.user_name = user_name
		per.save()
		return HttpResponse("Updated")

	def post(self, request, *args, **kwargs):
		_arg2 = request.POST
		first_name = _arg2.get('first_name')
		last_name = _arg2.get('last_name')
		user_name = _arg2.get('user_name')
		company_name = _arg2.get('company_name')

		try:
			get_company = Company.objects.get(company_name=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Post error")

		try:
			user_name = Person.objects.get(user_name=user_name)
		except ObjectDoesNotExist:
			p = Person.objects.create(first_name=first_name, last_name=last_name, user_name=user_name, company_name=get_company)
			return HttpResponse("Person's data inserted")
		return HttpResponse("User name already exists")

class CompanyView(View):
	def __init__(self):
		print 'Company View working'

	def get(self, request, *args, **kwargs):
		_params = request.GET
		company_array = Company.objects.all()
		data1=[]
		for i_company in company_array:
			data1.append({
				'company_name': i_company.company_name,
				'location_name': i_company.location,
				'pin_code_name': i_company.pin_code
				})
		return HttpResponse(json.dumps(data1), content_type='application/json')
	
	def post(self, request, *args, **kwargs):
		_arg3 = request.POST
		company_name = _arg3.get('company_name')
		location_name = _arg3.get('location')
		pin_code= _arg3.get('pin_code')

		try:
			get_company_name = Company.objects.get(company_name=company_name)
		except ObjectDoesNotExist:
			c = Company(company_name=company_name,location=location_name,pin_code=pin_code)
			c.save()
			return HttpResponse("Company's data inserted")
		return HttpResponse("Company name already exists")

	def put(self, request, *Args, **kwargs):
		#import pdb; pdb.set_trace()
		_arg4 = QueryDict(request.body)
		company_name=_arg4.get('company_name')
		location_name=_arg4.get('location')
		pin_code=_arg4.get('pin_code')
		try:
			ch=Company.objects.get(company_name=company_name)
		except ObjectDoesNotExist:
			return HttpResponse("Could not find company")

		ch.company_name=company_name
		ch.location=location_name
		ch.pin_code=pin_code
		ch.save()
		return HttpResponse("Company View Updated")
